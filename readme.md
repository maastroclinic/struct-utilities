# Matlab Structure Utilities #
this library contains basic functions which prevent re-coding of certain structure operations

## isFieldClass ##
the isfield() function in matlab checks the existance of a certain field for a given structure array. This function provides the same functionality for custom made value or handle classes

## orderStructureArray ##
sorts an structure array using a certain field of that array. <br />
**example:** <br />
a(1).index = 2; <br />
a(2).index = 3; <br />
a(3).index = 1; <br />
**will be sorted to:** <br />
a(1).index = 1; <br />
a(2).index = 2; <br />
a(3).index = 3; <br />